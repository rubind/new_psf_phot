from astropy.io import fits
from astropy import wcs
import sys
import numpy as np
from DavidsNM import save_img
sys.path.append("/Users/rubind/Dropbox/STScI/wfc3_psf/")
sys.path.append("/home/scpdata06/wfc3_psf/")
from wfc3_psf import get_sampled_PSF
import subprocess

def read_settings(pfl):
    f = open(pfl, 'r')
    lines = f.read().split('\n')
    f.close()

    for line in lines:
        parsed = line.split(None)
        if len(parsed) > 1:
            if parsed[0] == "images":
                images = eval(" ".join(parsed[1:]))
            if parsed[0] == "sciext":
                sciext = int(parsed[1])
            if parsed[0] == "RA0":
                RA0 = float(parsed[1])
            if parsed[0] == "Dec0":
                Dec0 = float(parsed[1])
            if parsed[0] == "oversample":
                oversample = int(parsed[1])
                
    return images, sciext, RA0, Dec0, oversample

def get_x0_y0(images, sciext, RA0, Dec0):
    x0 = []
    y0 = []
    filts = []

    for image in images:
        f = fits.open(image)
        w = wcs.WCS(f[sciext].header)
        filts.append(f[0].header["FILTER"])
        f.close()

        pix_xy = w.all_world2pix([[RA0, Dec0]], 1)[0]
        x0.append(int(np.around(pix_xy[0])))
        y0.append(int(np.around(pix_xy[1])))

    return x0, y0, filts

def write_PSFs(x0, y0, filts):
    psf_names = []
    
    for i in range(len(x0)):
        if subprocess.getoutput("hostname").count("David") > 0:
            the_path = "/Users/rubind/Dropbox/STScI/WFC3_PSF/"
        else:
            the_path = "/home/scpdata06/wfc3_psf/"

        the_PSF = get_sampled_PSF(filts[i], x0[i], y0[i], oversample, the_path = the_path)
        psf_names.append("PSF_%s_x=%04i_y=%04i_sub=%02i.fits" % (filts[i], x0[i], y0[i], oversample)) 
        save_img(the_PSF, psf_names[i])
    return psf_names

def update_settings(pfl, psf_names):
    f = open(pfl, 'r')
    lines = f.read().split('\n')
    f.close()

    for i in range(len(lines)):
        parsed = lines[i].split(None)
        if len(parsed) > 1:
            if parsed[0] == "psfs":
                lines[i] = "psfs\t\t\t" + str(psf_names)
            if parsed[0] == "psf_has_pix":
                lines[i] = "psf_has_pix\t\t1"
    f = open(pfl, 'w')
    f.write('\n'.join(lines))
    f.close()
    

images, sciext, RA0, Dec0, oversample = read_settings(sys.argv[1])
print (images, sciext, RA0, Dec0)

x0, y0, filts = get_x0_y0(images, sciext, RA0, Dec0)

print ("x0", x0)
print ("y0", y0)
print ("filts", filts)

psf_names = write_PSFs(x0, y0, filts)

update_settings(sys.argv[1], psf_names)
