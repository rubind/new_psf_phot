oversample		11		# Should match psf
renormpsf		0		# only use if psf is not normalized
psf_has_pix		0

fitSNoffset		0		# 1 => fit offset
patch			41		# patch size-- must be odd
fixmodelfromrefs	0		# 1=> fix the model from the references		
use_DQ_list    		0
n_cpu			8


sndRA_offset		0		# SNRA - GalRA
sndDec_offset		0		# SNdec - Galdec

RA0			[90.017621]		# Can be single number, or list		
Dec0			[-10.01567]		# Can be single number, or list

images			["epoch0.fits"]

sciext			1
errext			2
dqext			3
okaydqs			[0]
errscale		[1.]

pixel_area_map		"pam.fits"
bad_pixel_list		"bad_pixel_list.txt"

epochs			[0]		# 0 = reference, 1 = first epoch, ...
psfs			["12647_10mas.fits"]
apodize			0
n_iter			2
SN_centroid_prior_arcsec	0.1


<<<<<<< HEAD:paramfile.txt
=======
<<<<<<< HEAD:paramfile_old.txt


splineradius		19
splinepixelscale	0.00015          # default is 2.10e-5
>>>>>>> 756f00570d1efd27cc66d9cfc355b3d052da326a:paramfile_old.txt

splineradius		20
splinepixelscale	0.00003055          # default is 2.10e-5
